echo "Starting celery"
celery -A fake_csv worker --loglevel=INFO &
echo "Migrating database"
python manage.py migrate
echo "Collecting static files"
python manage.py collectstatic --noinput
echo "Starting server"
python manage.py runserver 0.0.0.0:8000 --noreload

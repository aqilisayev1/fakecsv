document.querySelectorAll(".delete-button").forEach((tag) => {
    tag.addEventListener("click", (e) => {
        const url = e.target.previousElementSibling.href
        const schemaId = parseInt(url.substring(url.lastIndexOf("/") + 1));

        axios.defaults.xsrfCookieName = 'csrftoken';
        axios.defaults.xsrfHeaderName = "X-CSRFTOKEN";

        axios.delete(
            `/api/schema/${schemaId}`,
            {
                headers: {
                    'Content-Type': 'application/json'
                }
            }
        ).then(function (response) {
            if (response.status == 204) {
                window.location = "/schemas/"
            } else {
                console.log(response.data)
            }
        }).catch(function (error) {
            console.log(error);
        });

    })
})

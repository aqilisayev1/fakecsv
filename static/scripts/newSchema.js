let optionValues = {
    "full_name": "Full name",
    "domain_name": "Domain name",
    "job": "Job",
    "email": "Email",
    "phone_number": "Phone number",
    "company": "Company",
    "address": "Address",
    "text": "Paragraph",
    "date": "Date",
    "integer": "Integer"
}


document.getElementById("new-schema-form").addEventListener("submit", function (event) {
    event.preventDefault();

    const state = {
        name: '',
        column_separator: '',
        string_character: '',
        columns: []
    };

    state["name"] = document.getElementById("schema-name").value;
    state["column_separator"] = document.getElementById("column-separator").value;
    state["string_character"] = document.getElementById("string-character").value;

    const addedColumns = document.getElementById("column-added-data").getElementsByClassName("column-data-row");

    for (let index = 0; index < addedColumns.length; index++) {
        let column = {}
        column["name"] = addedColumns[index].querySelector("#columnName").value;
        column["type"] = addedColumns[index].querySelector("#columnType").value;
        column["range_from"] = addedColumns[index].querySelector("#rangeFrom").value || null;
        column["range_to"] = addedColumns[index].querySelector("#rangeTo").value || null;
        column["order"] = addedColumns[index].querySelector("#columnOrder").value;
        state.columns.push(column);
    }

    axios.defaults.xsrfCookieName = 'csrftoken';
    axios.defaults.xsrfHeaderName = "X-CSRFTOKEN";

    console.log(state)
    axios.post(
        '/api/schemas/',
        state,
        {
            headers: {
                'Content-Type': 'application/json'
            }
        }
    ).then(function (response) {
        if (response.status == 201) {
            window.location = "/schemas/"
        } else {
            console.log(response.data)
        }
    }).catch(function (error) {
        console.log(error);
    });
});


document.getElementById("add-column-button").addEventListener("click", function (e) {
    const columnName = document.querySelector("#column-add-form #columnName");
    const columnType = document.querySelector("#column-add-form #columnType");
    const columnOrder = document.querySelector("#column-add-form #columnOrder");
    const columnFrom = document.querySelector("#column-add-form #rangeFrom");
    const columnTo = document.querySelector("#column-add-form #rangeTo");

    debugger;

    let columnFromValue = 0;
    let columnToValue = 0;

    if (columnFrom) {
        columnFromValue = columnFrom.value;
    }
    if (columnTo) {
        columnToValue = columnTo.value;
    }


    let columnTag = document.createElement("div");
    columnTag.classList.add("row");
    columnTag.classList.add("mb-4");
    columnTag.classList.add("column-data-row");
    let columnHtml = createColumnHtml(columnName.value, columnType.value, columnFromValue, columnToValue, columnOrder.value);
    columnTag.innerHTML = columnHtml;


    document.querySelector("#column-added-data").append(columnTag);
})


function createColumnHtml(name, type, from, to, order) {

    let option = optionValues[type]

    let column = `
    <div class="col-4">
        <label for="columnName" class="form-label">Column name</label>
        <input class="form-control column-input disabled" type="text" id="columnName" name="columnName" value="${name}">
    </div>
    <div class="col-3">
        <label for="columnType" class="form-label">Type</label>

        <select class="form-select column-input type-select disabled" id="columnType" value="${type}">
                <option value="${type}">${option}</option>
        </select>
    </div>
    <div class="col-1">
        <label for="rangeFrom" class="form-label">From</label>
        <input class="form-control column-input disabled" type="text" id="rangeFrom" name="rangeFrom" value="${from}">
    </div>
    <div class="col-1">
        <label for="rangeTo" class="form-label">To</label>
        <input class="form-control column-input disabled" type="text" id="rangeTo" name="rangeTo" value="${to}">
    </div>

    <div class="col-2">
        <label for="columnOrder" class="form-label">Order</label>
        <input class="form-control column-input disabled" type="text" id="columnOrder" name="order" value="${order}">
    </div>
    <div class="col-1 align-self-end">
        <button type="button" class="btn delete-button">Delete</button>
    </div>`;
    return column;
}


document.querySelector(".type-select").addEventListener("change", (e) => {
    const fromElement = document.createElement("div");
    const toElement = document.createElement("div");
    fromElement.innerHTML = ` 
            <label for="rangeFrom" class="form-label">From</label>
            <input class="form-control column-input" type="text" id="rangeFrom" name="rangeFrom">
        `
    toElement.innerHTML = ` 
            <label for="rangeTo" class="form-label">To</label>
            <input class="form-control column-input" type="text" id="rangeTo" name="rangeTo">
        `

    toElement.classList.add("col-6");
    fromElement.classList.add("col-6");

    const target = e.target;
    let rangeTag = target.parentElement.nextElementSibling;
    console.log(rangeTag);


    if (target.value === "integer" || target.value === "text") {
        if (rangeTag.innerHTML === "") {
            rangeTag.appendChild(fromElement);
            rangeTag.appendChild(toElement);
        }
    } else {
        rangeTag.innerHTML = "";
    }
})

document.addEventListener("click", (e) => {
    if (e.target.classList.contains("delete-button")) {
        e.target.parentElement.parentElement.remove();
    }
})
document.querySelector("#create-dataset").addEventListener("submit", (e) => {
    e.preventDefault()
    const rowCountTag = document.querySelector("#row-count");
    const url = window.location.href;
    const schemaId = parseInt(url.substring(url.lastIndexOf("/") + 1));

    const state = {
        "row_count": parseInt(rowCountTag.value),
        "schema": schemaId
    }

    axios.defaults.xsrfCookieName = 'csrftoken';
    axios.defaults.xsrfHeaderName = "X-CSRFTOKEN";

    axios.post(
        '/api/datasets/',
        state,
        {
            headers: {
                'Content-Type': 'application/json'
            }
        }
    ).then(function (response) {
        if (response.status == 201) {
            window.location = `/edit-schema/${schemaId}`
        } else {
            console.log(response.data)
        }
    }).catch(function (error) {
        console.log(error);
    });
})


from celery import Celery

from django.conf import settings

app = Celery('fake_csv')

app.conf.update(
    BROKER_URL=settings.CELERY_REDIS_URL,
    CELERY_RESULT_BACKEND=settings.CELERY_REDIS_URL
)
app.autodiscover_tasks()

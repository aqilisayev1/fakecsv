from storages.backends.gcloud import GoogleCloudStorage
from django.conf import settings


class MediaStorage(GoogleCloudStorage):
    bucket_name = "fake-csv"
    file_overwrite = False

    def path(self, name):
        return f"csv/{name}"

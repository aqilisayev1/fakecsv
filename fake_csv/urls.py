import os

from django.contrib import admin
from django.urls import path, include
from app.views import schema_view, new_schema_view, edit_schema_view
from django.contrib.auth import views as auth_view
from django.conf import settings
from django.views.static import serve
from django.conf.urls import url

urlpatterns = [
    path('admin/', admin.site.urls),
    path('login/', auth_view.LoginView.as_view(template_name="login.html", redirect_field_name="schemas"),
         name="login"),
    path('logout/', auth_view.LogoutView.as_view(template_name="login.html", redirect_field_name="login"),
         name="logout"),
    path('schemas/', schema_view, name="schemas"),
    path('new-schema/', new_schema_view, name="new-schema"),
    path('edit-schema/<int:pk>', edit_schema_view, name="edit-schema"),
    path('api/', include('api.urls')),
]


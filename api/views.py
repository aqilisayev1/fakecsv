from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from app.models import Schema
from .serializers import SchemaSerializer, DatasetSerializer
from .tasks import generate_fake_data


class SchemaListAPI(APIView):

    def post(self, request):
        request.data["user"] = request.user.id
        schema_serializer = SchemaSerializer(data=request.data)
        if schema_serializer.is_valid():
            schema_serializer.save()
            return Response(schema_serializer.data, status=status.HTTP_201_CREATED)

        return Response(schema_serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class SchemaAPI(APIView):

    def delete(self, request, pk):
        schema = Schema.objects.filter(pk=pk).first()
        if schema:
            schema.delete()
            return Response(status=status.HTTP_204_NO_CONTENT)

        return Response({"error": f"Couldn't find the schema with pk {pk}"}, status=status.HTTP_404_NOT_FOUND)


class DatasetAPI(APIView):

    def post(self, request):
        serializer = DatasetSerializer(data=request.data)
        if serializer.is_valid():
            dataset = serializer.save()
            generate_fake_data.delay(dataset.pk, dataset.schema.pk, dataset.schema.column_separator)
            return Response({"result": "Csv generation started"}, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

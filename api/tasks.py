import os
from django.core.files.base import ContentFile
from uuid import uuid4

from celery import shared_task

from app.models import Column, Dataset
import pandas as pd
from faker import Faker


@shared_task
def generate_fake_data(dataset_id: int, schema_id: int, col_sep: str):
    dataset = Dataset.objects.filter(pk=dataset_id).first()
    columns = Column.objects.filter(schema=schema_id)

    filename = str(uuid4())
    faker = Faker()
    data = {}

    for col in columns:
        data[col.name] = []
        for _ in range(dataset.row_count):
            if col.type == Column.INTEGER:
                data[col.name].append(faker.random_int(min=col.range_from, max=col.range_to))
            elif col.type == Column.EMAIL:
                data[col.name].append(faker.email())
            elif col.type == Column.JOB:
                data[col.name].append(faker.job())
            elif col.type == Column.DOMAIN_NAME:
                data[col.name].append(faker.domain_name())
            elif col.type == Column.FULL_NAME:
                data[col.name].append(faker.name())
            elif col.type == Column.PHONE_NUMBER:
                data[col.name].append(faker.phone_number())
            elif col.type == Column.COMPANY:
                data[col.name].append(faker.company())
            elif col.type == Column.ADDRESS:
                data[col.name].append(faker.address())
            elif col.type == Column.TEXT:
                data[col.name].append(faker.paragraph(nb_sentences=col.range_to))
            elif col.type == Column.DATE:
                data[col.name].append(faker.date())

    data_frame = pd.DataFrame(data)
    file = ContentFile(data_frame.to_csv(sep=col_sep).encode("utf-8"))
    dataset.status = True
    dataset.save()
    dataset.file.save(f"{filename}.csv", file)

from django.test import TransactionTestCase
from rest_framework.test import APIRequestFactory
from django.urls import reverse
from django.contrib.auth.models import User
from api.views import SchemaListAPI, SchemaAPI, DatasetAPI
from app.models import Schema, Column, Dataset


class TestSchemaListApiView(TransactionTestCase):
    reset_sequences = True

    @classmethod
    def setUpClass(cls):
        cls.request_factory = APIRequestFactory()
        cls.url = reverse("api-schemas")
        cls.user = User.objects.create_user(username="test_schema_list", password="pass123")

    @classmethod
    def tearDownClass(cls):
        pass

    def test_schema_list_POST_succeeds(self) -> None:
        data = {
            "name": "TestSchema",
            "column_separator": ",",
            "string_character": "\"",
            "columns": [
                {
                    "name": "FirstCol",
                    "type": "integer",
                    "range_from": 100,
                    "range_to": 200,
                    "order": 0
                },
                {
                    "name": "SecondCol",
                    "type": "job",
                    "order": 1
                }
            ]
        }

        request = self.request_factory.post(self.url, data, format="json")
        request.user = self.user

        response = SchemaListAPI.as_view()(request)

        self.assertEquals(response.status_code, 201, f"Could not create schema\n Error: {response.data}")

        schema = Schema.objects.first()
        self.assertIsNotNone(schema, "No schema in database")

        column_count = Column.objects.count()
        self.assertEquals(column_count, 2, "Columns are not created")
        self.assertEquals(schema.column_set.count(), 2, "Columns are not related to schema")


class TestSchemaApiView(TransactionTestCase):
    reset_sequences = True

    @classmethod
    def setUpClass(cls):
        cls.request_factory = APIRequestFactory()
        cls.url = reverse("api-schema", args=[1])
        cls.user = User.objects.create_user(username="test_schema", password="pass123")
        Schema.objects.create(name="Test", column_separator=",", string_character="\"", user=cls.user)

    @classmethod
    def tearDownClass(cls):
        pass

    def test_schema_DELETE_succeeds(self) -> None:
        request = self.request_factory.delete(self.url, format="json")
        request.user = self.user

        response = SchemaAPI.as_view()(request, 1)

        self.assertEquals(response.status_code, 204, f"Couldn't delete\n Error: {response.data}")
        self.assertEquals(Schema.objects.count(), 0, "Didn't delete from database")


class TestDatasetListApiView(TransactionTestCase):
    reset_sequences = True

    @classmethod
    def setUpClass(cls):
        cls.request_factory = APIRequestFactory()
        cls.url = reverse("api-datasets")
        cls.user = User.objects.create_user(username="test_datasets_list", password="pass123")
        cls.schema = Schema.objects.create(name="Test", column_separator=",", string_character="\"", user=cls.user)

    @classmethod
    def tearDownClass(cls):
        pass

    def test_dataset_POST_succeeds(self) -> None:
        data = {
            "schema": self.schema.pk,
            "row_count": 200
        }

        request = self.request_factory.post(self.url, data, format="json")
        request.user = self.user

        response = DatasetAPI.as_view()(request)

        self.assertEquals(response.status_code, 201)

        dataset = Dataset.objects.first()
        self.assertIsNotNone(dataset, "No dataset in database")
        self.assertEquals(dataset.schema.pk, self.schema.pk, "Database is not related to given schema")
        self.assertEquals(dataset.row_count, 200, "Row count is false")

from django.test import TestCase
from api.views import SchemaListAPI, SchemaAPI, DatasetAPI
from django.urls import reverse, resolve


class TestUrls(TestCase):

    def test_schema_list_api_resolves(self) -> None:
        url = reverse("api-schemas")
        self.assertEqual(resolve(url).func.view_class, SchemaListAPI)

    def test_schema_api_resolves(self) -> None:
        url = reverse("api-schema", args=[1])
        self.assertEqual(resolve(url).func.view_class, SchemaAPI)

    def test_dataset_list_api_resolves(self) -> None:
        url = reverse("api-datasets")
        self.assertEqual(resolve(url).func.view_class, DatasetAPI)

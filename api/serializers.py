from rest_framework import serializers
from app.models import Schema, Column, Dataset


class ColumnSerializer(serializers.ModelSerializer):
    class Meta:
        model = Column
        exclude = ("schema",)


class SchemaSerializer(serializers.ModelSerializer):
    columns = ColumnSerializer(many=True, required=False)

    class Meta:
        model = Schema
        fields = ["name", "string_character", "column_separator", "user", "columns"]

    def create(self, validated_data):
        columns_data = validated_data.pop("columns")
        schema = Schema.objects.create(**validated_data)
        for column_data in columns_data:
            Column.objects.create(schema=schema, **column_data)

        return schema


class DatasetSerializer(serializers.ModelSerializer):
    class Meta:
        model = Dataset
        exclude = ("file",)

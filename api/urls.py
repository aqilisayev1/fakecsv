from django.urls import path
from api.views import SchemaListAPI, SchemaAPI, DatasetAPI

urlpatterns = [
    path("schemas/", SchemaListAPI.as_view(), name="api-schemas"),
    path("schema/<int:pk>", SchemaAPI.as_view(), name="api-schema"),
    path("datasets/", DatasetAPI.as_view(), name="api-datasets"),
]

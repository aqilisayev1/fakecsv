from django.conf import settings
from django.db import models
from django.contrib.auth.models import User
from django.core.validators import MinValueValidator


# Create your models here.
class Schema(models.Model):
    COMMA = ","
    TAB = "\t"
    PIPE = "|"

    separator_choices = [
        (COMMA, "Comma"),
        (TAB, "Tab"),
        (PIPE, "Pipe")
    ]
    name = models.CharField(max_length=25)
    column_separator = models.CharField(max_length=5, choices=separator_choices)
    string_character = models.CharField(max_length=3)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    created_at = models.DateField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name


class Column(models.Model):
    FULL_NAME = "full_name"
    JOB = "job"
    EMAIL = "email"
    DOMAIN_NAME = "domain_name"
    PHONE_NUMBER = "phone_number"
    COMPANY = "company"
    TEXT = "text"
    INTEGER = "integer"
    ADDRESS = "address"
    DATE = "date"

    type_choices = [
        (FULL_NAME, "Full name"),
        (JOB, "Job"),
        (EMAIL, "Email"),
        (DOMAIN_NAME, "Domain name"),
        (PHONE_NUMBER, "Phone number"),
        (COMPANY, "Company"),
        (TEXT, "Text"),
        (INTEGER, "Integer"),
        (ADDRESS, "Address"),
        (DATE, "Date")
    ]

    name = models.CharField(max_length=25)
    type = models.CharField(max_length=25, choices=type_choices)
    order = models.IntegerField(validators=[MinValueValidator(0)])
    range_from = models.IntegerField(null=True)
    range_to = models.IntegerField(null=True)
    schema = models.ForeignKey(Schema, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class Dataset(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    status = models.BooleanField(default=False)
    schema = models.ForeignKey(Schema, on_delete=models.CASCADE)
    row_count = models.IntegerField(validators=[MinValueValidator(0)])
    file = models.FileField(upload_to="csv/", storage=settings.MEDIA_STORAGE)

    def __str__(self):
        return str(self.created_at)

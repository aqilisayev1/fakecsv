from django.contrib.auth.models import User
from django.test import TestCase

from app.models import Schema, Column


class TestModels(TestCase):

    @classmethod
    def setUpClass(cls):
        cls.user = User.objects.create_user(username="test_dataset_list", password="pass123")
        cls.schema = Schema.objects.create(name="Test", column_separator=",", string_character="\"", user=cls.user)
        cls.column = Column.objects.create(name="TestCol", type="integer", order=0, range_from=0, range_to=200,
                                           schema=cls.schema)

    @classmethod
    def tearDownClass(cls):
        pass

    def test_str_methods(self) -> None:
        self.assertEquals(str(self.schema), "Test")
        self.assertEquals(str(self.column), "TestCol")

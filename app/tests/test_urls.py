from django.test import TestCase
from app.views import schema_view, new_schema_view, edit_schema_view
from django.urls import reverse, resolve


class TestUrls(TestCase):

    def test_schema_view_resolves(self) -> None:
        url = reverse("schemas")
        self.assertEqual(resolve(url).func, schema_view)

    def test_new_schema_view_resolves(self) -> None:
        url = reverse("new-schema")
        self.assertEqual(resolve(url).func, new_schema_view)

    def test_edit_schema_view(self) -> None:
        url = reverse("edit-schema", args=[1])
        self.assertEqual(resolve(url).func, edit_schema_view)

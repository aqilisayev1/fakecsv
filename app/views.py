from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from .models import Schema, Dataset


@login_required(login_url="/login/")
def schema_view(request):
    schemas = Schema.objects.filter(user=request.user)
    return render(request, "schemas.html", {"schemas": schemas})


@login_required(login_url="/login/")
def new_schema_view(request):
    return render(request, "new_schema.html")


@login_required(login_url="/login/")
def edit_schema_view(request, pk):
    schema = Schema.objects.filter(pk=pk).first()

    if schema:
        datasets = Dataset.objects.filter(schema=schema)
        return render(request, "edit_schema.html", {"schema": schema, "datasets": datasets})

    return render(request, "schemas.html", {"error": "Couldn't find schema"}, status=404)

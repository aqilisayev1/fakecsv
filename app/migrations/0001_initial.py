# Generated by Django 3.2 on 2021-04-09 13:54

from django.conf import settings
import django.core.validators
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Schema',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=25)),
                ('column_separator',
                 models.CharField(choices=[(',', 'Comma'), ('\t', 'Tab'), ('|', 'Pipe')], max_length=5)),
                ('string_character', models.CharField(max_length=3)),
                ('created_at', models.DateField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Dataset',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('status', models.BooleanField()),
                ('row_count', models.IntegerField(validators=[django.core.validators.MinValueValidator(0)])),
                ('file', models.FileField(upload_to='csv')),
                ('schema', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app.schema')),
            ],
        ),
        migrations.CreateModel(
            name='Column',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=25)),
                ('type', models.CharField(choices=[('full_name', 'Full name'), ('job', 'Job'), ('email', 'Email'),
                                                   ('domain_name', 'Domain name'), ('phone_number', 'Phone number'),
                                                   ('company', 'Company'), ('text', 'Text'), ('integer', 'Integer'),
                                                   ('address', 'Address'), ('date', 'Date')], max_length=25)),
                ('order', models.IntegerField(validators=[django.core.validators.MinValueValidator(0)])),
                ('range', models.IntegerField()),
                ('schema', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app.schema')),
            ],
        ),
    ]
